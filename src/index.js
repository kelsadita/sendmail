#!/usr/bin/env node

import program from 'commander';
import MailBot from './mailBot';
import prompt from 'prompt';
import fs from 'fs';

let CONFIG_FILE_PATH = __dirname + '/../resources/config.json';

program
.version('0.0.1')
.description('Send email from command line')
.option('-a, --auth', 'Authorize email account')
.parse(process.argv);

let authenticationProperties = [{
  name: 'email',
  description: 'Your gmail email ID',
  required: true
}, {
  name: 'password',
  description: 'Password',
  required: true
}];

let emailProperties = [{
  name: 'from',
  description: 'From',
  required: true
}, {
  name: 'to',
  description: 'To',
  required: true
}, {
  name: 'subject',
  description: 'Subject',
  required: true
}, {
  name: 'body',
  description: 'Body',
}, {
  name: 'attachmentName',
  description: 'Name of attachment',
}, {
  name: 'attachmentPath',
  description: 'Full path of attachment',
}];

function createMail(result) {
  let emailOptions = {
    from        : result.from,
    to          : result.to,
    subject     : result.subject,
    text        : result.body || ''
  };

  // TODO: Add attachment path validation also support multiple attachment
  if (result.attachmentName && result.attachmentPath) {
    emailOptions.attachments = [
      {
        filename: result.attachmentName,
        path: result.attachmentPath
      }
    ];
  }

  return emailOptions;
}

function updateEmailAccount(result) {
  let email = result.email;
  let password = result.password;

  let fileData = fs.readFileSync(CONFIG_FILE_PATH);
  var updatedConfigs = fileData.toString()
    .replace(/\"username\": \"(.*?)\"/g, '\"username\": \"' + email + '\"')
    .replace(/\"password\": \"(.*?)\"/g, '\"password\": \"' + password + '\"');
  fs.writeFileSync(CONFIG_FILE_PATH, updatedConfigs);
}

// Check if the request is only for authorization
if (program.auth) {
  prompt.start();
  prompt.get(authenticationProperties, (err, result) => {
    if (err) {
      console.error('There was error in receiving inputs.');
    }
    updateEmailAccount(result);
    process.exit(1);
  });
} else {
  prompt.start();
  prompt.get(emailProperties, (err, result) => {
    if (err) {
      console.error('There was error in receiving inputs.');
    }
    new MailBot().sendMail(createMail(result));
  });
}
