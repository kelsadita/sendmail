import fs from 'fs';
import nodemailer from 'nodemailer';
import { Spinner } from 'clui';
import chalk from 'chalk';

class MailBot {
  constructor() {
    let config = JSON.parse(fs.readFileSync(__dirname + '/../resources/config.json'));
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: config.emailConfig.username,
        pass: config.emailConfig.password
      }
    });
  }

  sendMail(mail) {
    let countdown = new Spinner('Sending email...');
    countdown.start();
    this.transporter.sendMail(mail, (err) => {
      if (err) {
        let errorMessage = err.code === 'EAUTH' ?
        chalk.red('\nError: Please add your email credentials using `sendemail --auth`') :
        err.response;
        console.log(errorMessage);
      }
      process.exit(1);
    });
  }
}

export default MailBot;
