Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _clui = require('clui');

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var MailBot = (function () {
  function MailBot() {
    _classCallCheck(this, MailBot);

    var config = JSON.parse(_fs2['default'].readFileSync(__dirname + '/../resources/config.json'));
    this.transporter = _nodemailer2['default'].createTransport({
      service: 'gmail',
      auth: {
        user: config.emailConfig.username,
        pass: config.emailConfig.password
      }
    });
  }

  _createClass(MailBot, [{
    key: 'sendMail',
    value: function sendMail(mail) {
      var countdown = new _clui.Spinner('Sending email...');
      countdown.start();
      this.transporter.sendMail(mail, function (err) {
        if (err) {
          var errorMessage = err.code === 'EAUTH' ? _chalk2['default'].red('\nError: Please add your email credentials using `sendemail --auth`') : err.response;
          console.log(errorMessage);
        }
        process.exit(1);
      });
    }
  }]);

  return MailBot;
})();

exports['default'] = MailBot;
module.exports = exports['default'];