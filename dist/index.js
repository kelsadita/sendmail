#!/usr/bin/env node
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _mailBot = require('./mailBot');

var _mailBot2 = _interopRequireDefault(_mailBot);

var _prompt = require('prompt');

var _prompt2 = _interopRequireDefault(_prompt);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var CONFIG_FILE_PATH = __dirname + '/../resources/config.json';

_commander2['default'].version('0.0.1').description('Send email from command line').option('-a, --auth', 'Authorize email account').parse(process.argv);

var authenticationProperties = [{
  name: 'email',
  description: 'Your gmail email ID',
  required: true
}, {
  name: 'password',
  description: 'Password',
  required: true
}];

var emailProperties = [{
  name: 'from',
  description: 'From',
  required: true
}, {
  name: 'to',
  description: 'To',
  required: true
}, {
  name: 'subject',
  description: 'Subject',
  required: true
}, {
  name: 'body',
  description: 'Body'
}, {
  name: 'attachmentName',
  description: 'Name of attachment'
}, {
  name: 'attachmentPath',
  description: 'Full path of attachment'
}];

function createMail(result) {
  var emailOptions = {
    from: result.from,
    to: result.to,
    subject: result.subject,
    text: result.body || ''
  };

  // TODO: Add attachment path validation also support multiple attachment
  if (result.attachmentName && result.attachmentPath) {
    emailOptions.attachments = [{
      filename: result.attachmentName,
      path: result.attachmentPath
    }];
  }

  return emailOptions;
}

function updateEmailAccount(result) {
  var email = result.email;
  var password = result.password;

  var fileData = _fs2['default'].readFileSync(CONFIG_FILE_PATH);
  var updatedConfigs = fileData.toString().replace(/\"username\": \"(.*?)\"/g, '"username": "' + email + '"').replace(/\"password\": \"(.*?)\"/g, '"password": "' + password + '"');
  _fs2['default'].writeFileSync(CONFIG_FILE_PATH, updatedConfigs);
}

// Check if the request is only for authorization
if (_commander2['default'].auth) {
  _prompt2['default'].start();
  _prompt2['default'].get(authenticationProperties, function (err, result) {
    if (err) {
      console.error('There was error in receiving inputs.');
    }
    updateEmailAccount(result);
    process.exit(1);
  });
} else {
  _prompt2['default'].start();
  _prompt2['default'].get(emailProperties, function (err, result) {
    if (err) {
      console.error('There was error in receiving inputs.');
    }
    new _mailBot2['default']().sendMail(createMail(result));
  });
}