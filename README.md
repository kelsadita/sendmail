# SendMail

Sending emails from command line.

# Installation

```
npm install -g send-mail-cli
```

# Configuration

You will need to enter your Gmail email id and corresponding password for sending email.

```
sendemail --auth
```

# Sending email

```
sendemail
```

# Sample

```
➜  SendMail git:(master) ✗ sendemail
prompt: From:  tochen@gmail.com
prompt: To:  kalpesh.adhatrao@gmail.com
prompt: Subject:  Awesome Email
prompt: Body:  This is an Awesome Email
prompt: Name of attachment:  test.txt
prompt: Full path of attachment:  /Users/kalpeshadhatrao/Desktop/tochenfile.txt
  ◟ Sending email...
```
